from django.shortcuts import render
from .models.product import Product
# Create your views here.


def index(request):
    product = Product.objects.all()
    return render(request, 'index.html', {'Products_list': product})
